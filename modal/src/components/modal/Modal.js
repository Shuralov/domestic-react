import React, { Component } from 'react';
import "./Modal.scss";
class Modal extends Component {

    render() {
        if(!this.props.show){
            return null;
        }
        return <div className={'modal'}>
            <h1 className={'header'}>{this.props.header}{this.props.closeButton
                ? <span className={'closeButton'} onClick={this.props.modalController}/>
                : <span className={null} />
            }</h1>
            <p>{this.props.text}</p>
            {this.props.children}
        </div>;
    }
}
export default Modal;