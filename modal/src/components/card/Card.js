import React from 'react';
import "./Card.scss";

export class Card extends React.Component {
    render() {
        return (
            <div className={"card"}>
                <img src={this.props.img} alt="item"/>
                <p>
                    {this.props.cardName}
                </p>
                <p>
                    {this.props.cardPrice}
                </p>
                <p>
                    {this.props.cardArt}
                </p>
                <p>
                    {this.props.cardColor}
                </p>
                {this.props.children}
            </div>
        );
    }
}

export default Card;