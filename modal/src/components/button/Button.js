import React from 'react';
import "./Button.scss";

export class Button extends React.Component {
    render() {
        return (
            <button onClick={this.props.modalController}
                    style={{backgroundColor:this.props.bg}}
                    className={this.props.classname}
                    value={this.props.param}
            >
            {this.props.text}

            </button>
        );
    }
}

export default Button;